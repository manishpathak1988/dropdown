//
//  MPDropDownTableCellTableViewCell.swift
//  DropdownDemo
//
//  Created by MANISH_iOS on 09/03/16.
//  Copyright © 2016 iDev. All rights reserved.
//

import UIKit

class MPDropDownTableCellTableViewCell: UITableViewCell {

    @IBOutlet var imgV: UIImageView!
    
    @IBOutlet var lbl: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        self.layer.borderColor = UIColor.blackColor().CGColor
        self.layer.borderWidth = 0.30

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
