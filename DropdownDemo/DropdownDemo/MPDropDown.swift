//
//  MPDropDown.swift
//  DropdownDemo
//
//  Created by MANISH_iOS on 09/03/16.
//  Copyright © 2016 iDev. All rights reserved.
//

import UIKit
let kCellIdentifierMPDropDown: String = "menuTableCellId"

class MPDropDown: UITextField, UITableViewDelegate, UITableViewDataSource
{
    var myTableView : UITableView!

    var myArray : NSArray = NSArray()
    override func drawRect(rect: CGRect) {
        // Drawing code
    }

    required init(coder :NSCoder)
    {
        super.init(coder: coder)!
                    self.clipsToBounds = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "endFlow", name: "superViewTapped", object: self)
        
    }
    

    override func becomeFirstResponder() -> Bool
    {
        
        let returnValue = super.becomeFirstResponder()
        
        if returnValue == true
        
        {
            customizeTableView()
            createTableView()
        }
        return returnValue
    }
    func endFlow()
    {

    }
    override func didChangeValueForKey(key: String) {
        
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool // return NO to disallow editing.
    {
        return false
    }

    func textFieldDidBeginEditing(textField: UITextField) // became first responder
    {
        
    }


    func createTableView()
    {
        //PRINT A DETAILED VERSION OF THE FRAME, ALSO CAN PERFORM CALCULATION BETWEEN THE RECT PARAMETERS TO GET , FOR EXAMPLE, WIDTH POINT IN THE SUPERVIEW:
    dispatch_async(dispatch_get_main_queue())
        {
            if !self.myTableView.isDescendantOfView(self.superview!)
            {
                self.superview!.addSubview(self.myTableView)
            }
        }
    }
    func customizeTableView()
    {
        if myTableView != nil
        {
            let originInSuperview: CGPoint = self.superview!.convertPoint(CGPointZero, fromView: self)
            
            let yourFrame = self.convertRect(self.frame, toView: self.superview)
            
            //PRINT THE WHOLE RECT
            print(yourFrame)
            
            myTableView = UITableView(frame: CGRectMake(originInSuperview.x, originInSuperview.y + yourFrame.height, yourFrame.width, yourFrame.height * 5), style: UITableViewStyle.Plain)
            
            print(myTableView.frame)
            
            myTableView.delegate      =   self
            myTableView.dataSource    =   self
            let nib: UINib = UINib(nibName: "MPDropDownTableCellTableViewCell", bundle: nil)
            myTableView.registerNib(nib, forCellReuseIdentifier: kCellIdentifierMPDropDown)
            myTableView.backgroundColor = UIColor.cyanColor()
            self.superview!.addSubview(self.myTableView)
        }
    }
    
    //MARK:- Table view delegate and datasource
    // MARK: Table View Data Source
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
        return self.frame.height * 0.95
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell: MPDropDownTableCellTableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifierMPDropDown, forIndexPath: indexPath) as! MPDropDownTableCellTableViewCell
        cell.autoresizingMask = [UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleWidth]
        cell.contentView.clipsToBounds = true
        cell.backgroundColor = cell.contentView.backgroundColor
        cell.layoutIfNeeded()
        cell.lbl.text = "Test"
        cell.imgV.image = UIImage(named: "tImg")

        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.text = "Test set"
        self.leftView = UIImageView(image: UIImage(named:  "tImg"))
        self.leftViewMode = .Always
        self .resignFirstResponder()
        dispatch_async(dispatch_get_main_queue())
        {
            self.myTableView .removeFromSuperview()

        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.locationInView(self)
            // do something with your currentPoint
        }
    }
}
